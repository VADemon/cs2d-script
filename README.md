# cs2d-script
Collection of Lua scripts I made for CS2D

#####Mapexport
Export map and its resources to a folder for easy sharing

http://unrealsoftware.de/files_show.php?file=16267


#####Improved Print
Basically, it improves the standard built-in print function (multiple arguments and tab support)

http://unrealsoftware.de/files_show.php?file=13214


#####TooManyWeapons
TooManyItems alike script for CS2D

http://unrealsoftware.de/files_show.php?file=16041
